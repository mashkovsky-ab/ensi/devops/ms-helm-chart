# Default values for app.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

nameOverride: ""
fullnameOverride: ""

app:
  #deleteAfter: "+480h"
  image:
    repository: ""
    tag: ""
    pullPolicy: IfNotPresent
  resources:
    limits:
      memory: 512Mi
    requests:
      cpu: 100m
      memory: 128Mi
  env: {}
#    SOME_VALUE:
#      value: '1234'
#    SECRET_VALUE:
#      secret:
#        key: 'password'
#    EXTERNAL_SECRET_VALUE:
#      external_secret:
#        name: external-secret
#        key: 'password'
#    MULTILINE:
#      value: |
#        banana
#        ananas
#        public key
  secrets: {}
  ## extraVolumes and extraVolumeMounts allows you to mount other volumes
  ## Example Use Case: mount ssh private key from Secret
  extraVolumes: []
    #- name: shared
    #  emptyDir: {}
    #- name: newrelic-socket
    #  hostPath:
    #    path: /var/run/newrelic
    #    type: DirectoryOrCreate
  extraVolumeMounts: []
    #- name: shared
    #  mountPath: /var/nginx-shared
    #- name: newrelic-socket
    #  mountPath: /tmp/run/newrelic
  entrypoint:
    steps:
      - name: Newrelic config setup
        command: |
          if [ "${NEW_RELIC_LICENSE_KEY}" != "" ]; then
              sed -e "s/newrelic.license =.*/newrelic.license = ${NEW_RELIC_LICENSE_KEY}/" \
              -e "s/newrelic.appname =.*/newrelic.appname = ${NEW_RELIC_APP_NAME}/" \
              -i /usr/local/etc/php/conf.d/newrelic.ini
          fi
      - name: Use external newrelic daemon
        command: |
          if [ "${NEWRELIC_EXTERNAL}" != "" ]; then
              sed -e "s!;newrelic.daemon.address =.*!newrelic.daemon.address = /tmp/run/newrelic/newrelic.sock!" \
                  -e "s/;newrelic.daemon.dont_launch =.*/newrelic.daemon.dont_launch = 3/" \
                  -i /usr/local/etc/php/conf.d/newrelic.ini
          fi
      - name: Check storage
        command: |
          if [ "$APP_STORAGE_PATH" != "" ]; then
            if ! [ -e "$APP_STORAGE_PATH" ]; then
              echo "[$(date +"%Y-%m-%d %H:%M:%S")] entrypoint.INFO: Creating storage folder"
              mkdir -p "$APP_STORAGE_PATH"
            fi
            if ! [ -e "$APP_STORAGE_PATH"/framework ]; then
              echo "[$(date +"%Y-%m-%d %H:%M:%S")] entrypoint.INFO: Copying storage structure"
              cp -r /var/www/storage/* "$APP_STORAGE_PATH"/
              chmod -R 777 "$APP_STORAGE_PATH"
            fi
          fi
      - name: Ensi FS synlink
        command: |
          cd ${APP_STORAGE_PATH:-"/var/www/storage"}

          if [ ! -L ensi ] && [ -e /var/ensi_data ]; then
              echo "[$(date +"%Y-%m-%d %H:%M:%S")] entrypoint.INFO: Creating ensi fs symlink"
              ln -s /var/ensi_data ensi
          else
              echo "[$(date +"%Y-%m-%d %H:%M:%S")] entrypoint.INFO: Ensi FS symlink already exists"
          fi
      - name: Chmod bootstrap
        command: |
          cd /var/www
          [ -e bootstrap ] && chmod -R 777 bootstrap
      - name: Link storage
        command: |
          php artisan storage:link
      - name: Artisan optimize
        command: |
          php artisan optimize

hook:
  enabled: true
  container: php
  steps:
    - name: migrate-db
      command: |
        su -m -s /bin/sh www-data -c 'php artisan migrate --force'
  resources:
    limits:
      cpu: 2000m
      memory: 6Gi
    requests:
      cpu: 100m
      memory: 128Mi
  env:
    NEWRELIC_EXTERNAL:
      value: "true"

web:
  enabled: true
  container: php
  replicas: 1
  args:
    - "php-fpm"
    - "-R"
  lifecycle: {}
    # postStart:
    #   exec:
    #     command: [ "/bin/sh", "-c", "cp -r /var/www/public/* /var/nginx-shared/ && chmod -R 777 /var/nginx-shared" ]
  resources: {}
  env: {}

  service:
    name: crm
    type: ClusterIP
    port: 80
    targetPort: 8080
    labels: {}
      # prometheus.io/type: app

  ingress:
    enabled: false
    domain: ""
    base_domain: dev.ensi.tech
    annotations: { }
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
    tls: true
    tls_secret: ""

  autoscaling:
    enabled: false
    minReplicas: 1
    maxReplicas: 100
    targetCPUUtilizationPercentage: 80
    # targetMemoryUtilizationPercentage: 80

  nginx:
    image:
      repository: nginx
      tag: '1.19.3-alpine'
      pullPolicy: IfNotPresent
    resources:
      limits:
        memory: 300Mi
      requests:
        cpu: 10m
        memory: 50Mi
    ## extraVolumes and extraVolumeMounts allows you to mount other volumes
    ## Example Use Case: mount ssh private key from Secret
    extraVolumes: []
    #   - name: ssh-private-key
    #     secret:
    #       defaultMode: 420
    #       secretName: sftp-import-private-key
    extraVolumeMounts: []
      # - name: shared
      #   mountPath: /var/www/public
    config:
      http: |-
        sendfile on;
        keepalive_timeout 80;
        keepalive_requests 100000;
        reset_timedout_connection on;
        tcp_nopush on;
        tcp_nodelay on;
        open_file_cache max=200000 inactive=20s;
        open_file_cache_valid 30s;
        open_file_cache_min_uses 2;
        open_file_cache_errors on;
      server: |
        server {
            listen {{ .Values.web.service.targetPort }};
            server_name _ default;
            root /var/www/public;

            resolver ${RESOLVER} ipv6=off;

            index index.php;

            charset utf-8;

            client_max_body_size 500M;
            large_client_header_buffers 4 16k;
            client_body_buffer_size 64K;
            client_header_buffer_size 2k;
            http2_max_header_size 512k;
            http2_max_field_size 256k;
            disable_symlinks off;

            location / {
                try_files $uri $uri/ /index.php?$query_string;
            }

            error_page 404 /index.php;

            location ~ \.php$ {
                fastcgi_pass localhost:9000;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;
            }
        }

workers: {}
  # default:
  #   enabled: true
  #   container: php
  #   replicas: 1
  #   command: "php artisan queue:work --timeout=0 --once"
  #   resources: {}
  #   env:
  #     NEWRELIC_EXTERNAL:
  #       value: "true"

cronjobs: {}
  # elastic-scan:
  #   enabled: true
  #   container: php
  #   schedule: "* * * * *"
  #   command: ["php", "artisan", "index-requests:scan"]
  #   resources: {}
  #   env:
  #     NEWRELIC_EXTERNAL:
  #       value: "true"

imagePullSecrets: []
#  - name: regcred

serviceAccount:
  # Specifies whether a service account should be created
  create: false
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podAnnotations: {}

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

nodeSelector: {}

tolerations: []

affinity: {}
